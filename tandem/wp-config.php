<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'zOy!>#3;N:rlBv$ig$$9!HEp_VYGx9=JU|=XJ<u?GX$6ssCQIXXjd=[WR 4R IOt' );
define( 'SECURE_AUTH_KEY',  'L(5.#dwZR}[`npyqf0VJL;KV>{mCH>$x]#--3+2VL>EV*Ee Ni.^S:Tp}a4A:7{3' );
define( 'LOGGED_IN_KEY',    'mbZBbC]~M8j%{7eypzsB-U1MHXX-zfr>gt;~<8O3FHKd3/{+9;r{eUX3dZ[|kN3Z' );
define( 'NONCE_KEY',        'XOvp$Pqgd@)Ats;Q7>drijcGJ[Q%89$N^t#4uGi_Ubq[(tl(x9]vHC8.gqGtt~t}' );
define( 'AUTH_SALT',        'WLUUU| pc+xdWH%^S-VVO/A#&e@T%Xv3`d#sro]g6r$jw*qNsFNIR!(Zjd~/18ru' );
define( 'SECURE_AUTH_SALT', 'qywY*4H{);[SBwdlr%;neObD@6.1g87bBgEYy:*CnjL5L2R8G>qLm;Y5j}^~Gf{~' );
define( 'LOGGED_IN_SALT',   'a#k$D_5]+G-g33|P6c`$;B?-a[h,5r3X;D),dQ28%cRw?1L4;*-a-mDSIq?uwU?c' );
define( 'NONCE_SALT',       ',>2EuJUpydm)s&xcR@2h%=<=Pz4w!ykb62QW:LOwCXId/8+2hP`irpwyB3$0_[f=' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
